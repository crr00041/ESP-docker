#!/bin/bash

for f in $DOCKER_ENTRY_POINT*; do
	case "$f" in
		*setup-brimir.sh) echo "$0: running setup $f"; . "$f" ;;
		*.sh)  echo "$0: running $f"; . "$f" ;;
		*)     echo "$0: ignoring $f" ;;
	esac
done

bundle exec passenger start -a 0.0.0.0 -p 3000 -e production
