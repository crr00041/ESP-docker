# ESP-docker
Set of docker containers for work

##Images
[Brimir](github.com/ivaldi/brimir) - Brimir is a simple helpdesk system that can be used to handle support requests via incoming email. Brimir is currently used in production at Ivaldi.

[Postgres](postgresql.org) - Database for backing all the other services

[Taiga](github.com/taigaio) - Project management web application with scrum in mind! Built on top of Django and AngularJS (Front)

[ejabberd](github.com/processone/ejabberd) - Robust, ubiquitous and massively scalable Jabber / XMPP Instant Messaging platform


##Config
###Brimir
####Environment Variables
- `POSTGRES_HOST` - for the IP or address that postgres is going to be at. If you're linking to another container, this will be the container's name
- `DOCKER_ENTRY_POINT` - for the directory where you're going to be storing the pre-startup scripts. I do this through mounting a host volume.
- `DOMAIN` - for the domain that brimir is going to be housed at. This currently doesn't work and is a bug where you have to manually set each domain/tenant in the database
- `GENERATE_BRIMIR_DATABASE` - set this to have the generate_database script run and re-create the database.
##Progress
- [x] Brimir setup
- [x] Postgres setup
- [ ] Taiga setup
- [ ] ejabbered setup
