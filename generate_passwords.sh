#!/bin/bash

if [ -z $1 ]; then
	echo "Please enter amount of passwords to generate as the first parameter"
	exit 1
fi
numOfPasswords=$1

if [ -w $2 ]; then
	echo "Please enter path for output as the second parameter"
	exit 1
fi

output=$2

if ! [ -x "/bin/pwgen" ]; then
	echo "pwgen isn't installed"
	exit 1
fi

for i in `seq 1 $numOfPasswords`;
do
	echo `pwgen -0 -A -1 8` >> $output
done

