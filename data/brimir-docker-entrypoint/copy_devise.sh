file="$DOCKER_ENTRY_POINT/devise.rb"
if [[ -e $file ]]
then
cp --backup=simple --suffix=".$(date +%F_%l:%M:%S)" $file ~/brimir/config/initializers/
fi

